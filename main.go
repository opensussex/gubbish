package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"path"

	"github.com/ausrasul/hashgen"
	"github.com/gorilla/mux"
	scribble "github.com/nanobox-io/golang-scribble"
)

//Gubbish Type
type Gubbish struct {
	ID      string
	Content string
}

//JSONResponse Type
type JSONResponse struct {
	Error   bool
	Content string
}

func main() {

	fmt.Println("Ctrl-C exit!")
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", index)
	router.HandleFunc("/create", create)
	router.HandleFunc("/read/{gubbishId}", read)

	log.Fatal(http.ListenAndServe(":8080", router))
}

// Index our web home
func index(w http.ResponseWriter, r *http.Request) {
	p := path.Dir("./static/")
	// set header
	w.Header().Set("Content-type", "text/html")
	http.ServeFile(w, r, p)
}

//Create the new Gubbish
func create(w http.ResponseWriter, r *http.Request) {

	db := dbDriver()

	content := r.FormValue("content")
	hash := hashgen.Get(32)
	err := db.Write("storage", hash, Gubbish{ID: hash, Content: content})
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	w.Write(CreateJSONResponse(err, hash))
}

//Read a Gubbish
func read(w http.ResponseWriter, r *http.Request) {

	db := dbDriver()

	vars := mux.Vars(r)
	gubbishID := vars["gubbishId"]

	oneGubbish := Gubbish{}
	err := db.Read("storage", gubbishID, &oneGubbish)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	w.Write(CreateJSONResponse(err, oneGubbish.Content))
}

//DbDriver returns the db
func dbDriver() *scribble.Driver {
	dir := "./"
	db, err := scribble.New(dir, nil)
	if err != nil {
		fmt.Println("Error", err)
	}

	return db
}

//CreateJSONResponse returns a response
func CreateJSONResponse(errorType error, content string) []byte {
	checkError := false
	if errorType != nil {
		checkError = true
		content = "There seems to be a problem"
	}
	response := JSONResponse{checkError, content}
	r, _ := json.Marshal(response)

	return r
}
